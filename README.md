# Double Pendulum 

Simple double pendulum simulation using cinder.
In the following GIF the swinging pendulum is shown. Additionally the state
space (right) and the angles of the two pendulums (bottom) are shown. This gives
a broad overview. 

![](media/simulation.gif)

The integration of the equations of motion of the double pendulum is done using
the Runge-Kutte 4th order integration method. 

The double pendulum has nonlinearities in its equations of motion which is why
it is a system that is sensitive to initial condition e.g. it experiences chaos.
To visualize this, the next GIF shows several double pendulums which slight
changes in their initial conditions (the initial angles). 

As can be seen, the different pendulums show similar behavior at the start but
then they start to show very different behavior even though their initial
conditions were very similar (offset of maximum 0.1 rad). This is known as
chaotic behavior. 

![](media/chaos_visualization.gif)

Further visualization can be seen in the next pictures. Here the trace of two
different pendulums is shown (only the trace of the bottom mass). The initial
conditions only differ in the angle of the lower pendulum by 0.1 rad. The traces
are initially similar (darker part) and then they start to diverge heavily. This
again shows chaotic behavior. The data was collected over 15 seconds. 

![](media/trace1.png)

![](media/trace2.png)

## Dependencies

`Cinder`: refer to
https://github.com/cinder/Cinder/wiki/Cinder-for-Linux-%7C-Ubuntu-15.X-on-x86_64
for installation instructions

## Build and Run

In the Cinder project run

`cd <Project root>/proj/cmake && mkdir build && cd build && cmake .. && make && ./Debug/DoublePendulum/DoublePendulum`
