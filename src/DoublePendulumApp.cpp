#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/Vector.h"
#include <iostream>
#include "DoublePendulum.h"

using namespace ci;
using namespace ci::app;

class PendulumApp : public App {
    private: 
        DoublePendulum *p1; 
        DoublePendulum *p2; 
        DoublePendulum *p3; 
        DoublePendulum *p4; 
        DoublePendulum *p5; 
        DoublePendulum *p6; 
        DoublePendulum *p7; 
        DoublePendulum *p8; 
        int color1[3] = {0, 0, 1}; 
        int color2[3] = {0, 1, 1}; 
        float frameRate = 30.0;
    public: 
        void draw(); 
        void setup(); 
};

void prepareSettings(PendulumApp::Settings* settings){
    settings->setWindowSize(400, 400); 
    settings->setFrameRate(100.0f); 
    settings->setTitle("Double Pendulum Simulation"); 
}

void PendulumApp::setup(){
    // create pendulums
    p1 = new DoublePendulum(color1, color2, 1, 0.9, 3.14/2, 3.14/2); 
    p2 = new DoublePendulum(color1, color2, 1, 0.9, 3.14/2, 3.14/2 + 0.1); 
    p3 = new DoublePendulum(color1, color2, 1, 0.9, 3.14/2, 3.14/2 + 0.2); 
    p4 = new DoublePendulum(color1, color2, 1, 0.9, 3.14/2, 3.14/2 + 0.3); 
    p5 = new DoublePendulum(color1, color2, 1, 0.9, 3.14/2, 3.14/2 + 0.4); 
    p6 = new DoublePendulum(color1, color2, 1, 0.9, 3.14/2 + 0.1, 3.14/2); 
    p7 = new DoublePendulum(color1, color2, 1, 0.9, 3.14/2 + 0.2, 3.14/2); 
    p8 = new DoublePendulum(color1, color2, 1, 0.9, 3.14/2 + 0.3, 3.14/2); 
}

void PendulumApp::draw(){
    gl::enable(GL_SCISSOR_TEST);
    // simulate the DoublePendulum with visualization
    gl::viewport(glm::vec2(0, 0), glm::vec2(getWindowWidth(), getWindowHeight()));
    gl::clear(); 
    p2->updatePendulum(); 
    p2->updatePendulum(); 
    p3->updatePendulum(); 
    p4->updatePendulum(); 
    p5->updatePendulum(); 
    p6->updatePendulum(); 
    p7->updatePendulum(); 
    p8->updatePendulum(); 
}

// This line tells Cinder to actually create and run the application.
CINDER_APP(PendulumApp, RendererGl, prepareSettings)
